import os
import hashlib
import sys

fichero = 0
hashes = [0]

def presentacion():
	print()
	print('      ___      ____     ____    //   ) )                                            ')
	print('    //   ) ) //       //       ((         ___      ___      __      ___     / __    ')
	print('     ___/ / //__     //__        \\\     //___) ) //   ) ) //  ) ) //   ) ) //   ) ) ')
	print('   / ____/      ) ) //   ) )       ) ) //       //   / / //      //       //   / /  ')
	print('  / /____ ((___/ / ((___/ / ((___ / / ((____   ((___( ( //      ((____   //   / /   ')
	print()
	print()
	print('                               By Iakov P4vlichenko                                 ')
	print()
	print('                                    Version 1.0                                     ')
	print()
	print('                    GitLab: https://gitlab.com/iakov_p4vlichenko                    ')
	print()
	print('            Download more databases from: https://256search.blogspot.com/           ')



#Genera la lista de archivos del directorio seleccionado
def generarListaArchivos(directorio):
	listaArchivos=[]
	for nombre_directorio, dirs, ficheros in os.walk(directorio):
		for nombre_fichero in ficheros:
			#Ruta completa del archivo
			item_full_path = nombre_directorio+'/'+nombre_fichero
			listaArchivos.append(item_full_path)
	return listaArchivos


#compara la lista de archivos generada con una lista de hashes
def comparador(listaArchivos, bd_sha256, numero_Archivos):
	sha256 = 0
	x = 0
	numero = numero_Archivos
	resultado_url=[]
	resultado_hash=[]
	while x <= len(listaArchivos):
		print(' File '+str(x)+' of '+str(numero))
		try:
			with open(listaArchivos[x], 'rb') as f:
				y = 0
				bytes = f.read()
				hash = hashlib.sha256(bytes).hexdigest()
				f.close()
				sha256=hash
				archivo = open(bd_sha256, 'r')
				longitud = len(archivo.readlines())
				archivo.seek(0)
				while y < longitud:
					linea = archivo.readline()
					linea = str.rstrip(linea)
					if linea == hash:
						resultado_url.append(listaArchivos[x])
						resultado_hash.append(hash)
						y+=1
					y += 1
				archivo.close()
			x+=1
		except:
			x+=1
			continue
	print()
	print(' - OPERATION COMPLETED  -')
	print()
	v=0
	if len(resultado_url) < 1:
		print(' - NO COINCIDENCES FOUND -')
		print()
	elif len(resultado_url) > 0:
		print(' CHECK MATCHES:')
		print()
	while v < len(resultado_url):
		print(' '+resultado_url[v])
		print(' '+resultado_hash[v])
		print()
		v = v+1

#Guarda lista de archivos
def guardarListaDeArchivos(directorio, nombre_archivo):
	archivos=open(nombre_archivo, 'w')
	for nombre_directorio, dirs, ficheros in os.walk(directorio):
		for nombre_fichero in ficheros:
			#Ruta completa del archivo
			item_full_path = nombre_directorio+'/'+nombre_fichero
			archivos.write(item_full_path.rstrip())
			archivos.write('\n')
	archivos.close()

#Genera diccionario de hashes
def generarDiccionarioHash(directorio, nombre_archivo):
	diccionario = open (nombre_archivo, 'w')
	for nombre_directorio, dirs, ficheros in os.walk(directorio):
		for nombre_fichero in ficheros:
			#Ruta completa del archivo
			item_full_path = nombre_directorio+'/'+nombre_fichero
			archivo = item_full_path
			with open (archivo, 'rb') as f:
				bytes = f.read()
				hash = hashlib.sha256(bytes).hexdigest()
				diccionario.write(hash)
				diccionario.write('\n')
	diccionario.close()

def numeroDeArchivos(directorio):
	listaArchivos=[]
	numeroDeArchivos = 0
	for nombre_directorio, dirs, ficheros in os.walk(directorio):
		for nombre_fichero in ficheros:
			numeroDeArchivos=numeroDeArchivos+1
	return numeroDeArchivos


def imprimirResumen(directorio, baseDeDatos):
	print ("")
	print(' Directory: '+directorio)
	print ("")
	print(' Database: ' + baseDeDatos)
	print ("")

def menuAyuda(ejecutable):
	print()
	print('  HELP: ')
	print()
	print('  Find files:')
	print('      python3 '+ejecutable+' -f [files_path] [dictionary_path]')
	print()
	print('  Generate hash dictionary:')
	print('      python3 '+ejecutable+' -d [files_path] [dictionary_name]')
	print()
	print('  Generate file list:')
	print('      python3 '+ejecutable+' -l [files_path] [file_name]')
	print()
	print('  Help:')
	print('      python3 '+ejecutable+' -h || python 256Search.py --help')
	print()


argumento=sys.argv
presentacion()
try:
	if argumento[1] == '-f':
		imprimirResumen(argumento[2], argumento[3])
		print(' Files to scan: '+str(numeroDeArchivos(argumento[2])))
		print()
		listaArchivos=generarListaArchivos(argumento[2])
		comparador(listaArchivos, argumento[3], numeroDeArchivos(argumento[2]))

	elif argumento[1] == '-d':
		print(' Working...')
		print()
		generarDiccionarioHash(argumento[2], argumento[3])

	elif argumento[1] == '-l':
		print(' Working...')
		print()
		guardarListaDeArchivos(argumento[2], argumento[3])

	elif argumento[1] == '-h' or argumento[1] == '--help':
		menuAyuda(argumento[0])

	else:
		print('ERROR!')

except:
	menuAyuda(argumento[0])
